# -----------> ACTIVITY SOLUTION <-----------
from abc import ABC, abstractclassmethod
from os import stat

# ---> [Parent Class]
class Person():
    @abstractclassmethod
    def get_full_name(self):
        pass

    @abstractclassmethod
    def add_request(self):
        pass

    @abstractclassmethod
    def check_request(self):
        pass

    @abstractclassmethod
    def add_user(self):
        pass

# ---> [Child Class]
class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    # Getters/Setters
    def get_first_name(self):
        return self._first_name
    def set_first_name(self, first_name):
        self._first_name = first_name

    def get_last_name(self):
        return self._last_name
    def set_last_name(self, last_name):
        self._last_name = last_name

    def get_email(self):
        return self._email
    def set_email(self, email):
        self._email = email

    def get_department(self):
        return self._department
    def set_department(self, department):
        self._department = department

    # Methods
    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'
    
    def add_request(self):
        return 'Request has been added'
    
    def check_request(self):
        return
    
    def add_user(self):
        return

    def login(self):
        return f'{self._email} has logged in'
    
    def logout(self):
        return f'{self._email} has logged out'


class Team_Lead(Person):
    def __init__(self, first_name, last_name, email, department, members):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = [members]
    
    # Getters/Setters
    def get_first_name(self):
        return self._first_name
    def set_first_name(self, first_name):
        self._first_name = first_name

    def get_last_name(self):
        return self._last_name
    def set_last_name(self, last_name):
        self._last_name = last_name

    def get_email(self):
        return self._email
    def set_email(self, email):
        self._email = email

    def get_department(self):
        return self._department
    def set_department(self, department):
        self._department = department

    def get_members(self):
        return self._members
    def set_members(self, members):
        self._members = members

    # Methods
    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'
    
    def add_request(self):
        return 
    
    def check_request(self):
        return 'Request has been checked.'
    
    def add_user(self):
        return

    def login(self):
        return f'{self._email} has logged in'
    
    def logout(self):
        return f'{self._email} has logged out'
    
    def add_member(self, members):
        return self._members.append(self._first_name)


class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    # Getters/Setters
    def get_first_name(self):
        return self._first_name
    def set_first_name(self, first_name):
        self._first_name = first_name

    def get_last_name(self):
        return self._last_name
    def set_last_name(self, last_name):
        self._last_name = last_name

    def get_email(self):
        return self._email
    def set_email(self, email):
        self._email = email

    def get_department(self):
        return self._department
    def set_department(self, department):
        self._department = department

    # Methods
    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'
    
    def add_request(self):
        return
    
    def check_request(self):
        return
    
    def add_user(self):
        return 'User has been added'

    def login(self):
        return f'{self._email} has logged in'
    
    def logout(self):
        return f'{self._email} has logged out'

class Request():
    def __init__(self, name, requester, date_requested, status):
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self._status = status

    # Getters/Setters
    def get_name(self):
        return self._name
    def set_name(self, name):
        self._name = name

    def get_requester(self):
        return self._requester
    def set_requester(self, requester):
        self._requester = requester
    
    def get_date_requested(self):
        return self._date_requested
    def set_date_requested(self, date_requested):
        self._date_requested = date_requested
    
    def get_status(self):
        return self._status
    def set_status(self, status):
        self._status = status

    # Methods:
    def update_request(self):
        return f'Request {self._name} has been {self._status}.'
    
    def close_request(self):
        return f'Request {self._name} has been {self._status}.'
    
    def cancel_request(self):
        return f'Request {self._name} has been {self._status}.'

# [TEST CASES]
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")

team_lead1 = Team_Lead("Michael", "Specter", "smichael@mail.com", "Sales", "Test")
req1 = Request("New hire orientation", team_lead1, "27-Jul-2021", "Test")
req2 = Request("Laptop repair", employee1, "1-Jul-2021", "Test")

assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert team_lead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

team_lead1.add_member(employee3)
team_lead1.add_member(employee4)
for indiv_emp in team_lead1.get_members():
    print(team_lead1.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())
